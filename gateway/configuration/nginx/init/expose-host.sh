#!/bin/sh
# vim:sw=4:ts=4:et

HOST_IP=$(ip route | awk 'NR==1 {print $3}')
HOST_DOMAIN="host.docker.internal"

ping -q -c1 "$HOST_DOMAIN" > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "Adding the $HOST_DOMAIN to the hosts file"
  echo -e "$HOST_IP\t$HOST_DOMAIN" >> /etc/hosts
fi

echo "============================"
echo " "
cat /etc/hosts 
echo " "
echo "============================"
