# Localsearch CH - Tech Exercise

Technical exercise as part of the localsearch.ch recruitment

# Requirements
- Docker
- Node / Npm

# Instructions:

## Installation
- run `npm i`

## Local staging
- run `npm run start` to spin up the nginx proxy, the server and the client applications.
- then browse into `http://localhost:8080`
- run `npm run stop` to stop the nginx proxy

## Quality Reporting
- run `npm run client:test` to output the client unit test coverage.
- run `npm run server:test` to output the server unit test coverage.

## Continuos Integration
- run `cd client && npm run test -- --watchAll` to monitor the client tests
- run `cd server && npm run test -- --watchAll` to monitor the server tests
