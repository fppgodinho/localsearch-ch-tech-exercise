import path from 'path';

export default {
    collectCoverageFrom: [ 'src/**/*.js' ],
    moduleFileExtensions: [ 'js' ],
    rootDir: process.cwd(),
    roots: [ '<rootDir>' ],
    testEnvironment: 'node',
    testRegex: 'src/.*.spec.js',
    verbose: true
};
