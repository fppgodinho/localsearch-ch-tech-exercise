import { mapScheduleIntervals } from './map-schedule-intervals';
import { serializeWeekSchedule } from './serialize-week-schedule';

export const mapSchedule = days => {
    const schedule = serializeWeekSchedule(days);

    return mapScheduleIntervals(schedule);
};
