import { serializeDaySchedule } from './serialize-day-schedule';
import { keys } from './week-days';

export const serializeWeekSchedule = days => keys.reduce((schedule, id) => ({
    ...schedule,
    [id]: days[id] && serializeDaySchedule(days[id]) || 'closed'
}), {});
