import { keys } from './week-days';

export const mapScheduleIntervals = schedule => keys.reduce((intervals, day) => {
    const id = intervals.length - 1;
    const interval =  intervals[id];
    const { times, days } = interval || {};

    if (schedule[day] !== times) return [
        ...intervals,
        { times: schedule[day], days: [ day ] }
    ];

    interval.days = [ ...days, day ];

    return [ ...intervals ];
}, []);
