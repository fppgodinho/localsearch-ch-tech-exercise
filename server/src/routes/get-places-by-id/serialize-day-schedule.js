export const serializeDaySchedule = schedule => schedule.map(({start, end}) => `${ start }-${ end }`).join(',');
