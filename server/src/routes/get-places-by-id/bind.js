import axios from 'axios';

import { mapSchedule } from './map-schedule';

export const bind = app => app.get('/getPlacesById/:id', ({ params: { id } }, res) => {
    axios.get(`https://storage.googleapis.com/coding-session-rest-api/${ id }`).then(({
        data,
        status
    }) => {
        if (status !== 200) throw Error(`Error Code: ${status}`);

        const { displayed_what: what, displayed_where: where, opening_hours } = data;

        const { days } = opening_hours;
        const schedule = mapSchedule(days);

        const response = {
            what,
            where,
            schedule
        }

        res.send(JSON.stringify(response));
    }).catch(error => {
        console.error(error);

        res.send(JSON.stringify(error));
    });
});
