'use strict';

export const bind = app => app.all('*', (req, {}, next) => {
    console.log('*** A request ***');
    console.log('method: ' + req.method);
    console.log('url: ' + req.url);
    console.log('*****************');

    next();
});
