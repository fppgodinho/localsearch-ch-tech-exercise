import express from 'express';

import { bind as bindLogAllRequests } from './routes/log-all-requests/bind';
import { bind as bindGetPlacesById } from './routes/get-places-by-id/bind';

const app = express();
bindLogAllRequests(app);
bindGetPlacesById(app);

app.listen(3000, '0.0.0.0');
