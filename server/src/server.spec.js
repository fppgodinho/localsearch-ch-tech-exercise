const mockedExpress = jest.fn();
const mockedListen = jest.fn();
const mockedUse = jest.fn();
const mockedExpressServer = {
    listen: mockedListen,
    use: mockedUse
};

const mockedBindLogAllRequests = jest.fn();
const mockedBindGetPlacesById = jest.fn();

const mockedLog = jest.spyOn(console, 'log');

describe('Given the server entry point', () => {
    beforeEach(() => {
        jest.doMock('express', () => mockedExpress);

        jest.doMock('./routes/log-all-requests/bind', () => ({ bind: mockedBindLogAllRequests }));
        jest.doMock('./routes/get-places-by-id/bind', () => ({ bind: mockedBindGetPlacesById }));

        mockedExpress.mockImplementation(() => mockedExpressServer);
        mockedLog.mockImplementation(() => {});
    });

    describe('When imported as a Node module', () => {
        beforeEach(() => {
            require('./server.js');
        });

        it('Then it should have created a single express server', () => {
            expect(mockedExpress).toHaveBeenCalledTimes(1);
        });

        describe('And the log all requests route', () => {
            let actualSubject = mockedBindLogAllRequests;

            it('Then it should have been bound a single time', () => {
                expect(actualSubject).toHaveBeenCalledTimes(1);
            });

            it('Then it should have been bound to the expected server', () => {
                expect(actualSubject).toHaveBeenCalledWith(mockedExpressServer);
            });
        });

        describe('And the get places by id route', () => {
            let actualSubject = mockedBindGetPlacesById;

            it('Then it should have been bound a single time', () => {
                expect(actualSubject).toHaveBeenCalledTimes(1);
            });

            it('Then it should have been bound to the expected server', () => {
                expect(actualSubject).toHaveBeenCalledWith(mockedExpressServer);
            });
        });

        describe('And the express server', () => {
            let actualSubject = mockedExpressServer;

            it('Then it should bet set to listen a single time', () => {
                expect(actualSubject.listen).toHaveBeenCalledTimes(1);
            });

            it('Then it should be set to listen the expected port and address', () => {
                expect(actualSubject.listen).toHaveBeenCalledWith(3000, '0.0.0.0');
            });
        });
    });
});
