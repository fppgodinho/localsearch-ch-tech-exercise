
import { mapSchedule } from './routes/get-places-by-id/map-schedule';

const sample1 = {
    "days": {
        "tuesday": [
            {
                "start": "11:30",
                "end": "15:00",
                "type": "OPEN"
            },
            {
                "start": "18:30",
                "end": "00:00",
                "type": "OPEN"
            }
        ],
        "wednesday": [
            {
                "start": "11:30",
                "end": "15:00",
                "type": "OPEN"
            },
            {
                "start": "18:30",
                "end": "00:00",
                "type": "OPEN"
            }
        ],
        "thursday": [
            {
                "start": "11:30",
                "end": "15:00",
                "type": "OPEN"
            },
            {
                "start": "18:30",
                "end": "00:00",
                "type": "OPEN"
            }
        ],
        "friday": [
            {
                "start": "11:30",
                "end": "15:00",
                "type": "OPEN"
            },
            {
                "start": "18:30",
                "end": "00:00",
                "type": "OPEN"
            }
        ],
        "saturday": [
            {
                "start": "18:00",
                "end": "00:00",
                "type": "OPEN"
            }
        ],
        "sunday": [
            {
                "start": "11:30",
                "end": "15:00",
                "type": "OPEN"
            }
        ]
    }
};

const sample2 = {
    "days": {
        "monday": [
            {
                "start": "11:30",
                "end": "14:00",
                "type": "OPEN"
            },
            {
                "start": "18:30",
                "end": "22:00",
                "type": "OPEN"
            }
        ],
        "tuesday": [
            {
                "start": "11:30",
                "end": "14:00",
                "type": "OPEN"
            },
            {
                "start": "18:30",
                "end": "22:00",
                "type": "OPEN"
            }
        ],
        "wednesday": [
            {
                "start": "11:30",
                "end": "14:00",
                "type": "OPEN"
            },
            {
                "start": "18:30",
                "end": "21:00",
                "type": "OPEN"
            }
        ],
        "thursday": [
            {
                "start": "11:30",
                "end": "14:00",
                "type": "OPEN"
            },
            {
                "start": "18:30",
                "end": "22:00",
                "type": "OPEN"
            }
        ],
        "friday": [
            {
                "start": "11:30",
                "end": "14:00",
                "type": "OPEN"
            },
            {
                "start": "18:30",
                "end": "22:00",
                "type": "OPEN"
            }
        ]
    },
    "closed_on_holidays": true,
    "open_by_arrangement": false
};

console.log('--==--==>', JSON.stringify(mapSchedule(sample1.days), undefined, 4));
console.log('--==--==>', JSON.stringify(mapSchedule(sample2.days), undefined, 4));
