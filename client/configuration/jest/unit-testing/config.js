

const path = require('path');

module.exports = {
    verbose: true,
    rootDir: path.resolve(__dirname, '../../../'),
    clearMocks: true,
    roots: [ '<rootDir>' ],
    setupFiles: ['<rootDir>/configuration/jest/unit-testing/setup.js'],
    testEnvironment: 'jsdom',
    testEnvironmentOptions : {
        url: 'http://dnd-maestro.net'
    },
    moduleFileExtensions: [ 'js', 'jsx', 'json' ],
    testRegex: 'src/.*.spec.jsx?',
    collectCoverageFrom: [ 'src/**/*.jsx?' ],
    moduleNameMapper: {
        '^.+\\.(css|less|scss)$': 'identity-obj-proxy'
    },
    clearMocks: true,
};
