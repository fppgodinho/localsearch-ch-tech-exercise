const webpack = require('webpack');

const versionsPlugin = new webpack.DefinePlugin({
    VERSION: JSON.stringify(require('../../package.json').version)
});

const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');

const statics = path.resolve(`${ __dirname }../../../statics/`);

const htmlWebPackPlugin = new HtmlWebpackPlugin({
    filename: 'index.html',
    template: path.resolve(`${ __dirname }../../../src/index.html`)
});

const cssLoader = () => ({
    loader: 'css-loader',
    options: { modules: true }
});

const postCSSLoader = () => ({
    loader: 'postcss-loader',
    options: {
        postcssOptions: {
            plugins: [
                [ 'autoprefixer', {} ]
            ]
        }
    }
});

const resolveUrlLoader = () => ({
    loader: 'resolve-url-loader',
    options: {
        sourceMap: true,
        debug: false
    }
});

const sassLoader = () => ({
    loader: 'sass-loader',
    options: { sourceMap: true }
});

exports.admin = {
    entry: path.resolve(`${ __dirname }../../../src/index.js`),
    resolve: {
        extensions: [
            '.js',
            '.jsx'
        ]
    },
    module: {
        rules: [
            {
                test: /\.scss$/i,
                use: [
                    'style-loader',
                    cssLoader(),
                    postCSSLoader(),
                    resolveUrlLoader(),
                    sassLoader()
                ]
            },
            {
                test: /\.jsx?$/,
                use: [ 'babel-loader' ]
            }
        ]
    },
    plugins: [
        htmlWebPackPlugin,
        versionsPlugin
    ],
    output: {
        publicPath: '/client/',
        path: statics,
        filename: 'bundle/chunks/[name].[contenthash].min.js',
        assetModuleFilename: 'bundle/assets/[name].[contenthash].min.js',
        clean: true
    },
    devServer: {
        static: {
            directory: statics
        },
        port: 8888,
        host: '0.0.0.0',
        allowedHosts: 'all',
        headers: {
            'Cache-Control': 'max-age=300'
        }
    }
};
