import React from 'react';

import styles from './styles.scss';

export const AppView = ({
    data,
    id,
    isOpen,
    loading,
    setId
}) => (
    <div className={ `component ${ styles.component }` }>
        <select
            className={ `selector ${ styles.selector }` }
            onChange={ ({ target }) => setId(target.value) }
            value={ id }
        >
            <option value="">Select</option>

            <option value="GXvPAor1ifNfpF0U5PTG0w">GXvPAor1ifNfpF0U5PTG0w</option>

            <option value="ohGSnJtMIC5nPfYRi_HTAg">ohGSnJtMIC5nPfYRi_HTAg</option>
        </select>

        { data && (
            <div className={ `data ${ styles.data }` }>
                <div className={ `details ${ styles.details }` }>
                    <div className={ `title ${ styles.title }` }>{ data.what }</div>

                    <div>{ data.where }</div>

                    <div className={ `state ${ styles.state } ${ isOpen ? 'open' : 'closed' }` }>{ isOpen ? 'open' : 'closed' }</div>
                </div>

                <div className={ `schedule ${ styles.schedule }` }>
                    <div className={ `title ${ styles.title }` }>Opening Hours</div>
                    {
                        data.schedule.map(({ times, days }) => (
                            <div
                                className={ `entry ${ styles.entry }` }
                                key={ `${ times }-${ days.join(',') }`
                            }>
                                { days.length > 1 && <div>{ `${ days[0] }-${ days[ days.length - 1 ] }` }</div> }

                                { days.length == 1 && <div>{ `${ days[0] }` }</div> }


                                <div>{ times.split(',').map(interval => (
                                    <div key={ interval }>{ interval }</div>
                                )) }</div>
                            </div>
                        ))
                    }
                </div>
            </div>
        ) }

        { loading && (
            <div className={ `loading ${ styles.loading }` }>Loading...</div>
        ) }
    </div>
);
