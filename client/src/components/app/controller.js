import React, { useEffect, useState } from 'react';

import { AppView } from './view';

export const keys = [ 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday' ];

const trailingZeros = value => `00${ value }`.substr(-2);

const getValidSchedule = (schedule, weekday) => schedule.filter(({ days }) => days.includes(weekday));

const enhanceData = data => {
    const { schedule } = data;
    const currentTime = new Date()
    const hours = trailingZeros(currentTime.getHours());
    const minutes = trailingZeros(currentTime.getMinutes());
    const weekday = keys[currentTime.getUTCDay() - 1];
    // const currentHours = `${ hours }:${ minutes }`;
    const currentHours = `19:00`;

    const validSchedule = getValidSchedule(schedule, weekday);


    let validInterval = undefined;

    validSchedule.forEach(({ times }) => {
        if (times === 'closed') return false;

        return times.split(',').forEach(interval => {
            const [ start, end ] = interval.split('-');

            if (currentHours > start && currentHours < end) validInterval = [ start, end ] ;
        });
    });

    console.log('--==--==>', validInterval);

    return validInterval;


    // return validSchedule.reduce((activePeriod, { times }) => {
    //     if (activePeriod) return activePeriod;

    //     if (times === 'closed') return false;

    //     return times.split(',').reduce((isOpenInterval, interval) => {
    //         if (isOpenInterval) return isOpenInterval;

    //         const [ start, end ] = interval.split('-');

    //         return (currentHours > start && currentHours < end) ? [start, end] : false;
    //     }, false);
    // }, false);
};

const getNextOpeningTime = ({ schedule }) => {
    const currentTime = new Date()
    const weekday = keys[currentTime.getUTCDay() - 1];

};

const getNextClosingTime = ({ schedule }) => {
    const currentTime = new Date()
    const weekday = keys[currentTime.getUTCDay() - 1];

    const validSchedule = getValidSchedule(schedule, weekday);



};

export const AppController = () => {
    const [ id, setId ] = useState();
    const [ data, setData ] = useState();
    const [ loading, setLoading ] = useState(false);
    const [ isOpen, setIsOpen ] = useState(false);
    const [ nextStateChangeTime, setNextStateChangeTime ] = useState(false);

    useEffect(() => {
        setData();

        if (!id) return;

        setLoading(true);

        fetch(`/api/getPlacesById/${ id }`).then(
            response => response.json()
        ).then(data => {
            const isOpen = enhanceData(data);

            setNextStateChangeTime(getNextClosingTime(data));

            setLoading(false);

            setIsOpen(isOpen);
            // setIsOpen(enhanceData({
            //     "what": "Casa Ferlin",
            //     "where": "Stampfenbachstrasse 38, 8006 Zürich",
            //     "schedule": [
            //         {
            //             "times": "09:00-14:00,18:30-22:00",
            //             "days": [
            //                 "monday",
            //                 "tuesday",
            //                 "wednesday",
            //                 "thursday",
            //                 "friday"
            //             ]
            //         },
            //         {
            //             "times": "closed",
            //             "days": [
            //                 "saturday",
            //                 "sunday"
            //             ]
            //         }
            //     ]
            // }));

            setData(data);
        });
    }, [ id ]);

    const props = {
        data,
        id,
        isOpen,
        loading,
        setId
    };

    return <AppView { ...props } />;
};
