import React from 'react';

const mockedCreateRoot = jest.fn();
const mockedRender = jest.fn();

const MockedApp = props => <div { ...props } data-id="SomeMockedApp" />

const mockedElement = 'some mocked element';

describe('Given the client entry point', () => {
    beforeEach(() => {
        jest.doMock('react-dom/client', () => ({
            createRoot: mockedCreateRoot
        }));

        jest.doMock('./components/app', () => ({ App: MockedApp }))

        mockedCreateRoot.mockImplementation(() => ({ render: mockedRender }))
    });

    describe('When imported as a Node module', () => {
        beforeEach(() => {
            require('./index');
        });

        it('Then it should have declared a global rendered', () => {
            expect(window.render).toBeInstanceOf(Function);
        });

        describe('And when rendered with an element', () => {
            beforeEach(() => {
                window.render(mockedElement);
            });

            it('Then it have generated a single root element', () => {
                expect(mockedCreateRoot).toHaveBeenCalledTimes(1);
            });

            it('Then it have generated the expected root element', () => {
                expect(mockedCreateRoot).toHaveBeenCalledWith(mockedElement);
            });

            it('Then it should have rendered a single time', () => {
                expect(mockedRender).toHaveBeenCalledTimes(1);
            });

            it('Then it should have rendered the expected component', () => {
                expect(mockedRender).toHaveBeenCalledWith(<MockedApp />);
            });
        });
    });
});