import React from 'react';
import { createRoot } from 'react-dom/client';

import { App } from './components/app';

window.render = element => {
    createRoot(element).render( <App />);
};
